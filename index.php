<?php
require_once 'functions.php';
require_once 'header.php';


if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS'){
    return 0;
}

try{
    $db = openDb();
  
    $sql = "select * from task";
    $query = $db->query($sql);
    $results = $query->fetchAll(PDO::FETCH_ASSOC);

    
    header('HTTP/1.1 200 OK');
    print json_encode($results);

} catch (PDOException $pdoex) {
    header('HTTP/1.1 500 Internal Server Error');
    $error = array('error' => $pdoex->getMessage());
    print json_encode($error);
}

